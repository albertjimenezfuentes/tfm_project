from mpl_toolkits.mplot3d import Axes3D
from ludwig import LudwigModel


input_f = [{"name": "CALL_TYPE",
            "type": "category"}, {"name": "DAY_TYPE", "type": "category"}, {"name": "TAXI_ID","type": "numerical"}]
output_f = [{"name": "TIMESTAMP",
            "type": "numerical"}]
model = LudwigModel({
    "input_features": input_f,
    "output_features": output_f
})
train_stats = model.train(data_csv="../dataset_test/test_time.csv")

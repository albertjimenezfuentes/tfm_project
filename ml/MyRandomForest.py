import numpy as np
from joblib import dump
from pandas import DataFrame
from sklearn.ensemble import RandomForestRegressor

from cleaner.mycleaner import MyCleaner
from util.BaseClass import BaseClass


class MyRandomForest(BaseClass):
    def __init__(self, dataset: BaseClass, n_estimators=1000, random_state=42):
        self.dataset = dataset.get_dataset()
        self.rf = RandomForestRegressor(n_estimators=n_estimators, random_state=random_state)

    def get_dataset(self) -> DataFrame:
        return self.dataset

    def train(self, save_model_file: bool = False):
        """
        Trains the model with the dataset provided and writes to a file optionally the generated model.
        Returns the predicted_values and the test_values to be visualized with the accuracy


        :param save_model_file: Boolean flag to turn on or off the model saving to a file
        :type: save_model_file: bool
        :returns: Returns the predicted_values and the test_values to be visualized
        :rtype: list
        """
        train_features, test_features, train_labels, test_labels = MyCleaner.prepare_for_training(self.dataset)
        rf = RandomForestRegressor(n_estimators=1000, random_state=42)
        # Train the model on training data
        rf.fit(train_features, train_labels)
        if save_model_file:
            dump(rf, "/Users/Beruto/PycharmProjects/TFM_Project/models/random_forest_model.joblib")

        predictions = rf.predict(test_features)
        # Calculate the absolute errors
        errors = abs(predictions - test_labels)
        # Print out the mean absolute error (mae)
        # print('Mean Absolute Error:', round(np.mean(errors), 2), 'degrees.')
        mape = 100 * (errors / test_labels)
        # Calculate and display accuracy
        accuracy = 100 - np.mean(mape)
        print('Accuracy:', round(accuracy, 2), '%.')
        return predictions, test_labels, accuracy

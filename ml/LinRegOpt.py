import json

import numpy as np
import pandas as pd
from haversine import haversine
from pandas import DataFrame
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split


class LinRegOpt:
    """
    Class for coding a LinearRegression model that predicts the location of the taxi ride.
    """
    infinity_val = float("inf")
    CHUNKSIZE = 100000
    MIN_POINTS = 30

    my_dtypes = {
        "TRIP_ID": np.int64,
        "CALL_TYPE": np.object,
        "ORIGIN_CALL": np.float64,
        "ORIGIN_STAND": np.float64,
        "TAXI_ID": np.int64,
        "TIMESTAMP": np.int64,
        "DAY_TYPE": np.object,
        "MISSING_DATA": np.bool,
        "POLYLINE": np.object
    }

    def __init__(self, training_dataset_filename: str, testing_dataset: DataFrame, min_points=MIN_POINTS):
        """
        Constructor for reading the training dataset using chunksizes so only the filename is needed, and the
        testing dataset
        :param training_dataset_filename: Filename of the training dataset
        :type: str
        :param testing_dataset: DataFrame of the of the testing dataset
        :type: DataFrame
        """
        self.training_dataset_filename = training_dataset_filename
        self.testing_dataset = testing_dataset
        LinRegOpt.MIN_POINTS = min_points
        self.list_models_latitude, self.list_models_longitude = self._obtain_models()

    def _train_fit_trajectory(self, min_points: int, train_latitude: bool) -> "list":
        """
        Private method for training the LinearRegression model using the training dataset.
        It will be read using the CHUNKSIZE option of Pandas because the entire dataset does not fit into RAM.
        :param min_points: Minimum amount of points that will be extracted from the GPS traces. Usually values between
        30 and 100 works pretty good.
        :type: int
        :param train_latitude: Boolean flag to train the X latitude coordinates if it is set to True or to train
        the Longitude if it is set to False
        :type: bool
        :return: A Generator that yields Linear Regression models. 18 models will be generated using training_dataset file
        :rtype: list
        """
        lr = LinearRegression()
        if train_latitude:
            print("Training Latitude - X")
        else:
            print("Training Longitude - Y")
        score_count = 1

        for dataset in pd.read_csv(self.training_dataset_filename, dtype=LinRegOpt.my_dtypes,
                                   chunksize=LinRegOpt.CHUNKSIZE):
            dataset = dataset.drop(dataset[dataset.MISSING_DATA == True].index)
            dataset = dataset["POLYLINE"].map(lambda x: json.loads(x))
            filtered_dataset = list(filter(lambda x: len(x) > LinRegOpt.MIN_POINTS * 2, dataset.to_list()))
            initial_points = list(map(lambda x: x[:min_points + 1] + x[-min_points:-1], filtered_dataset))
            initial_points = list(map(lambda x: [_[0] if train_latitude else _[1] for _ in x], initial_points))
            last_points = list(map(lambda x: x[-1][0] if train_latitude else x[-1][1], filtered_dataset))
            del filtered_dataset

            X_train, X_test, y_train, y_test = train_test_split(initial_points, last_points, test_size=0.33, )
            # X_train, X_test, y_train, y_test = list(map(np.array, [X_train, X_test, y_train, y_test]))
            model = lr.fit(X_train, y_train)
            print("Score achieved", lr.score(X_test, y_test), " Model Iteration: ", score_count)
            print(lr.intercept_, lr.get_params(), lr.coef_)
            score_count += 1
            yield model

    def _obtain_models(self) -> "tuple[list, list]":
        """
        Another private method that calls _train_fit_trajectory and retrieves the two list of models, for
        latitude and longitude.
        :return: tuple of two lists containing 18 models per list, first list latitude, second list longitude.
        :rtype: tuple
        """
        list_models_latitude = list(
            self._train_fit_trajectory(min_points=LinRegOpt.MIN_POINTS, train_latitude=True, ))
        list_models_longitude = list(
            self._train_fit_trajectory(min_points=LinRegOpt.MIN_POINTS, train_latitude=False, ))
        return list_models_latitude, list_models_longitude

    def test_dataset(self) -> "list[list]":
        """
        Public method that will call the rest of the methods to train, fit and predict using the test dataset as a target.
        After that it will return the list of best predicted coordinates and the actual coordinates. To do so,
        it will compare across the 18 models of latitude and 18 models of longitude, which combination is the one that
        minimizes the distance between the predicted point and the actual current point.
        :return: A list of lists with the best GPS points and the actual GPS points to be ready for being visualizated.
        :rtype: list
        """
        testing_dataset = self.testing_dataset
        testing_dataset = testing_dataset["POLYLINE"]
        testing_dataset = testing_dataset.map(lambda x: json.loads(x))
        testing_dataset = list(filter(lambda x: len(x) > LinRegOpt.MIN_POINTS * 2, testing_dataset.to_list()))
        list_models_latitude, list_models_longitude = self.list_models_latitude, self.list_models_longitude
        best_coords = (-1, -1)
        list_best_coords, list_actual_coords, best_distances = [], [], []

        for i, test in enumerate(testing_dataset):
            array_test_latitude = np.array([test[i][0] for i in range(LinRegOpt.MIN_POINTS * 2)]).reshape(1, -1)
            array_test_longitude = np.array([test[i][1] for i in range(LinRegOpt.MIN_POINTS * 2)]).reshape(1, -1)
            # obtain the minimum distance of the coordinate
            min_distance = LinRegOpt.infinity_val
            actual_coords = (array_test_latitude[-1][-1], array_test_longitude[-1][-1])

            for lat_m in list_models_latitude:
                for lon_m in list_models_longitude:
                    pred_coords = (lat_m.predict(array_test_latitude), lon_m.predict(array_test_longitude))
                    distance = haversine(pred_coords, actual_coords, unit="m")
                    if distance < min_distance:
                        min_distance = distance
                        best_coords = pred_coords

            print("predicted ", best_coords, " | ", " actual", actual_coords)
            print("Error in meters +-", round(min_distance, 2), "\n")
            list_best_coords.append(best_coords)
            list_actual_coords.append(actual_coords)
            best_distances.append(min_distance)

        return list_best_coords, list_actual_coords, best_distances

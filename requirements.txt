matplotlib
numpy
scipy
scikit-learn
flask
flask-cors
pandas
ludwig
joblib
folium
haversine
tensorflow
keras
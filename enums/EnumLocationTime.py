class EnumLocationTime:
    """
    Enum for selecting the time dataset or the location
    """
    TIME, LOCATION = range(2)

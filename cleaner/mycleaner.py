from pandas import DataFrame
from sklearn.model_selection import train_test_split

from enums.EnumLocationTime import EnumLocationTime
from util.BaseClass import BaseClass
from util.DatasetLoader import MyDatasetLoader
from util.MyUtilities import DatasetAttributes


class MyCleaner(BaseClass):
    """
    MyCleaner class responsible to clean the dataset and detect outliers along with some preprocessing
    """

    def __init__(self, my_dataset_loader: MyDatasetLoader, enum_location_time: int):
        """
        Initializes the Cleaning process at the construction time, takes a while
        :param my_dataset_loader:
        :param enum_location_time:
        """
        self.sub = my_dataset_loader.get_dataset()
        self.enum_location_time = enum_location_time
        self._clean()

    def get_dataset(self) -> DataFrame:
        """
        Inherited method to export the dataset to any class at runtime
        :return: Dataframe Dataset
        """
        return self.sub

    def _clean(self):
        self.sub = self.sub.drop(self.sub[self.sub.MISSING_DATA == True].index)
        self.sub = self.sub.drop_duplicates()
        self.sub.drop(DatasetAttributes.TRIP_ID, axis=1, inplace=True)
        self.sub.drop(DatasetAttributes.MISSING_DATA, axis=1, inplace=True)
        self.sub[DatasetAttributes.ORIGIN_CALL].fillna(0, inplace=True)
        self.sub[DatasetAttributes.ORIGIN_STAND].fillna(0, inplace=True)

    @staticmethod
    def prepare_for_training(dataset: DataFrame, enum_location_time: int = EnumLocationTime.TIME) -> (
            DataFrame, DataFrame, DataFrame, DataFrame):
        """
        Prepares the dataset for the training process, splitting it and returns 4 datasets,
        the training features, the testing features, the training features and the testing labels


        :param dataset: Any BaseClass instance to extract the dataset loaded inside it
        :type: dataset: DataFrame
        :param enum_location_time: The type of problem is it being tackled
        :type: enum_location_time: int
        :returns: tuple of 4 datasets
        :rtype: tuple
        """
        if enum_location_time == EnumLocationTime.TIME:
            labels = dataset[DatasetAttributes.TIMESTAMP]
            features = dataset.drop(
                [DatasetAttributes.TIMESTAMP, DatasetAttributes.POLYLINE, DatasetAttributes.DAY_TYPE,
                 DatasetAttributes.CALL_TYPE], axis=1)
            train_features, test_features, train_labels, test_labels = train_test_split(features, labels,
                                                                                        test_size=0.25, random_state=42)
            return train_features, test_features, train_labels, test_labels
        else:
            pass
            # TODO EnumLocationTime.LOCATION

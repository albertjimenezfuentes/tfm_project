from time import time

from cleaner.mycleaner import MyCleaner
from enums.EnumLocationTime import EnumLocationTime
from ml.LinRegOpt import LinRegOpt
from ml.MyRandomForest import MyRandomForest
from nn.MyCNN import MyCNN
from util import MyUtilities
from util.DatasetLoader import MyDatasetLoader
from visualizer.myvisualizer import MyVisualizer

if __name__ == '__main__':
    lt = time()
    MyUtilities.check_min_version_python()
    # DatasetLoader
    my_dataset_loader = MyDatasetLoader("/Users/Beruto/PycharmProjects/TFM_Project/dataset_train/small_train_time.csv")
    # my_dataset_loader = MyDatasetLoader("/Users/Beruto/PycharmProjects/TFM_Project/dataset_test/test_time.csv")
    # Cleaning
    cleaner = MyCleaner(my_dataset_loader, EnumLocationTime.TIME)

    # ML - RandomForest
    p, a, acc = MyRandomForest(cleaner).train(True)
    # Visualizing
    visualizer = MyVisualizer(cleaner)
    visualizer.plot_random_forest_results(p, a, acc, True)
    visualizer.histogram_call_type(save_fig=True)
    visualizer.bar_plot_call_type(save_fig=True)
    # Linear Regression Location
    testing = MyDatasetLoader("/Users/Beruto/PycharmProjects/TFM_Project/dataset_test/test_trajectory.csv")
    train_trajectory_filename = "/Users/Beruto/PycharmProjects/TFM_Project/dataset_train/train_trajectory.csv"
    LinRegOpt(train_trajectory_filename, testing.get_dataset()).test_dataset()
    # mean_error = []
    # for lin_reg_opt in [LinRegOpt(train_trajectory_filename, testing.get_dataset(), 20), LinRegOpt(train_trajectory_filename, testing.get_dataset(), 30), LinRegOpt(train_trajectory_filename, testing.get_dataset(), 50)]:
    #     best_coords, actual_coords, best_distances = lin_reg_opt.test_dataset()
    #     mean_error.append(mean(best_distances))
    # MyVisualizer.plot_linear_regression_best_distances([30, 40,60], mean_error)

    # CNN TIME
    for elem in MyCNN("/Users/Beruto/PycharmProjects/TFM_Project/dataset_test/test_time.csv").cnn_time():
        print(elem.history)
    lt_1 = time()
    print(f"Time elapsed -> {lt_1 - lt}")

from pandas import read_csv, DataFrame

from util.BaseClass import BaseClass


class MyDatasetLoader(BaseClass):
    """
    Loader of the dataset
    """

    def __init__(self, filename: str):
        """
        Loads the dataset in memory
        :param filename:
        """
        self.dataset = read_csv(filename)

    def get_dataset(self) -> DataFrame:
        """
        Inherited method to export the dataset to any class at runtime
        :returns: The loaded Dataset
        :rtype: DataFrame
        """
        return self.dataset

    def __del__(self):
        print(f"Destruction of {self.__class__.__name__}")
        self.dataset = None

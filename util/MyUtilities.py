from sys import version_info


class DatasetAttributes:
    """
    Stores in public class variables the STRING attributes of the CSV header
    """
    TRIP_ID = "TRIP_ID"
    CALL_TYPE = "CALL_TYPE"
    ORIGIN_CALL = "ORIGIN_CALL"
    ORIGIN_STAND = "ORIGIN_STAND"
    TAXI_ID = "TAXI_ID"
    TIMESTAMP = "TIMESTAMP"
    DAY_TYPE = "DAY_TYPE"
    MISSING_DATA = "MISSING_DATA"
    POLYLINE = "POLYLINE"


def check_min_version_python():
    """
    Check at runtime the python version ensuring that is the right version
    :exception: Exception if the version is below Python 3
    :return:
    """
    if version_info[0] < 3:
        raise Exception(
            "Current version of python {}{} is not supported, please upgrade to 3.6 or newer".format(version_info[0],
                                                                                                     version_info[1]))

from abc import ABC, abstractmethod

from pandas import DataFrame


class BaseClass(ABC):
    """
    BaseClass abstract class with one method to export the dataset
    """

    @abstractmethod
    def get_dataset(self) -> DataFrame:
        """
        Inherited method to export the dataset to any class at runtime
        :return: Dataframe Dataset
        """
        pass

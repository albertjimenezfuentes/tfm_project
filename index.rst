.. TaxiML documentation master file, created by
sphinx-quickstart on Mon Mar 18 01:32:22 2019.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.

Welcome to TaxiML's documentation!
==================================
.. toctree::
   :maxdepth: 2
   :caption: Contents:

Cleaner
=====================
The Cleaner package is responsible to clean, preprocess and prepare the dataset for the training phase.


.. automodule:: cleaner.mycleaner
   :members:

Machine Learning
=====================
Machine Learning package which stores the non-neural network algorithms. Right now there is a RandomForest implementation.


.. automodule:: ml.MyRandomForest
   :members:

.. automodule:: ml.LinRegOpt
   :members:

Neural Networks
=====================
Neural networks package that contains a class for instantiate and create a Tensorflow model using Convolutional Neural
Networks.


.. automodule:: nn.MyCNN
   :members:


Util
=====================
This package provides utilities, such as inheritance for methods, a DatasetLoader for referencing the dataset in one single piece of memory.


.. automodule:: util.MyUtilities
   :members:
.. automodule:: util.BaseClass
   :members:
.. automodule:: util.DatasetLoader
   :members:


Enums
=====================
Enumerations to distinguish between options at the time of coding the different problems.


.. automodule:: enums.EnumLocationTime
   :members:

Visualizer
=====================
Visualizer class that will plot and save (optionally) the results.


.. automodule:: visualizer.myvisualizer
   :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

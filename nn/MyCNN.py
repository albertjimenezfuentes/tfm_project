import json

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from haversine import haversine
from numpy.random import seed
from pandas import DataFrame
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.wrappers.scikit_learn import KerasRegressor


class MyCNN:
    """
    Convolutional Neural Network for regression and predict the estimated time that a taxi ride will take to arrive.
    Uses tensorflow Dense and 10-K Cross-fold validation plus mean squared error
    """
    CHUNKSIZE = 100000
    MIN_POINTS = 60
    seed = 10
    NUM_EPOCHS = 50
    np.random.seed(seed)
    my_dtypes = {
        "TRIP_ID": np.int64,
        "CALL_TYPE": np.object,
        "ORIGIN_CALL": np.float64,
        "ORIGIN_STAND": np.float64,
        "TAXI_ID": np.int64,
        "TIMESTAMP": np.int64,
        "DAY_TYPE": np.object,
        "MISSING_DATA": np.bool,
        "POLYLINE": np.object
    }

    def __init__(self, filename: str):
        """
        Due to the nature of the dataset, it needs to be loaded twice so it should be passed the filename
        instead of the entire dataset
        :param filename:
        """
        self.filename = filename

    def _baseline_model(self, input_shape: int = 2) -> "Sequential":
        """
        Private method for creating a baseline model, the proper tuning for the hidden layer has been done
        and this is the optimal configuration
        :param input_shape: The shape or number of dimensions of the feature dataset
        :return: A compiled tensorflow model
        :rtype: Sequential
        """
        model = Sequential()
        model.add(Dense(128, input_dim=input_shape, kernel_initializer='normal', activation='relu'))
        model.add(Dense(64, kernel_initializer='normal', activation='relu'))
        model.add(Dense(1, kernel_initializer='normal', activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae', 'accuracy'])
        model.summary()
        return model

    def _process_polyline(self, a_dataset: DataFrame) -> "list[float]":
        """
        Process the GPS points to reduce the 2D array of points to a single float value which is the sum of the distance
        that has been done so far.
        :param a_dataset: Testing or training dataset
        :type: DataFrame
        :return: A generator of one float by row containing the sum of the distances using haversian metric
        :rtype: list
        """
        coords = a_dataset["POLYLINE"].map(lambda x: json.loads(x))
        for i, row in enumerate(coords):
            sum_haversine = 0
            for index, elem in enumerate(row):
                if index >= len(row) - 3: break  # Break before the last 3 points
                sum_haversine += haversine(elem, row[index + 1])
            yield sum_haversine

    def cnn_time(self) -> "list":
        """
        Initialize the Convolutional neural network, applies the K Cross fold-validation and returns the results
        :return: Array of scores of the estimator for each run of the cross validation.
        :rtype: list
        """
        # I train with the features -> TAXI_ID, POLYLINE
        for dataset in pd.read_csv(self.filename,
                                   chunksize=MyCNN.CHUNKSIZE):
            dataset = dataset.drop(dataset[dataset.MISSING_DATA == True].index)
            dataset["POLYLINE"] = list(self._process_polyline(dataset))
            features = dataset[["POLYLINE", "TAXI_ID"]]
            target = dataset["TIMESTAMP"]
            estimator = KerasRegressor(build_fn=self._baseline_model, epochs=MyCNN.NUM_EPOCHS, batch_size=32, verbose=1)
            kfold = KFold(n_splits=10, random_state=seed)
            # results = cross_val_score(estimator, MinMaxScaler().fit_transform(features), target, cv=kfold)
            X_train, X_test, y_train, y_test = train_test_split(features, target, test_size=0.33, )
            results = estimator.fit(X_train, y_train)
            y_pred = estimator.predict(X_test)
            fig, ax = plt.subplots()
            ax.scatter(y_test, y_pred)
            ax.plot([y_test.min(), y_test.max()], [y_test.min(), y_test.max()], 'k--', lw=4)
            ax.set_xlabel('Measured')
            ax.set_ylabel('Predicted')
            plt.show()

            # print("Results: %.2f (%.2f) MSE" % (results.mean(), results.std()))
            yield results
            # return results

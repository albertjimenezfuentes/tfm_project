import json
from collections import Counter

import numpy as np
from matplotlib import pyplot as plt
from pandas import DataFrame

from util.BaseClass import BaseClass
from util.MyUtilities import DatasetAttributes


class MyVisualizer(BaseClass):
    """
    Visualizer class, takes the dataset and generates the proper files in /plots
    """

    def __init__(self, dataset_loader: BaseClass):
        self.dataset = dataset_loader.get_dataset()

    def get_dataset(self) -> DataFrame:
        """
        Inherited method to export the dataset to any class at runtime


        :returns: The dataset used for initializing this class
        :rtype: DataFrame
        """
        return self.dataset

    def heatmap(self, title: str = "Train heatmap", save_fig: bool = False):
        """
        Shows and saves if it is set up, a **heatmap** of the POLYLINE attribute in the dataset.


        :param title: Title of the plot
        :param save_fig: Boolean indicating if it is need to be saved the plot to a file or not
        :type: title: str
        :type save_fig: bool
        :returns: None
        :rtype: None
        """
        self.dataset[DatasetAttributes.POLYLINE] = self.dataset[DatasetAttributes.POLYLINE].map(
            lambda x: json.loads(x)[-1:])
        latlong = np.array([[p[0][1], p[0][0]] for p in self.dataset[DatasetAttributes.POLYLINE] if len(p) > 0])

        # cut off long distance trips
        lat_low, lat_hgh = np.percentile(latlong[:, 0], [2, 98])
        lon_low, lon_hgh = np.percentile(latlong[:, 1], [2, 98])

        # create image
        bins = 513
        lat_bins = np.linspace(lat_low, lat_hgh, bins)
        lon_bins = np.linspace(lon_low, lon_hgh, bins)
        H2, _, _ = np.histogram2d(latlong[:, 0], latlong[:, 1], bins=(lat_bins, lon_bins))

        img = np.log(H2[::-1, :] + 1)

        plt.figure()
        ax = plt.subplot(1, 1, 1)
        plt.imshow(img)
        plt.axis('off')
        plt.title(title)
        if save_fig:
            plt.savefig("./plots/heatmap_location.png")
        plt.show()

    def histogram_call_type(self, title: str = "Call type histogram", save_fig: bool = False):
        """
        Creates a histogram using the CALL_TYPE attribute


        :param title: Title of the plot
        :param save_fig: Boolean to save or not the plot into a file
        :type: title: str
        :type: save_fig: bool
        :return:
        """
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        count_hist = Counter(list(self.dataset[DatasetAttributes.CALL_TYPE].values)).items()
        ax.hist(count_hist, bins=3)
        plt.title(title)
        plt.xlabel('Number of CALL_TYPES')
        plt.ylabel('Frequency')
        if save_fig:
            plt.savefig("./plots/histogram_call_type_train.png")
        plt.show()

    def bar_plot_call_type(self, title: str = "Call type bar plot", save_fig: bool = False):
        """
        Creates a bar plot using the CALL_TYPE attribute


        :param title: Title of the plot
        :param save_fig: Boolean to save or not the plot into a file
        :type: title: str
        :type: save_fig: bool
        :return:
        """
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        count_hist = Counter(list(self.dataset[DatasetAttributes.CALL_TYPE].values))
        ax.plot(list(count_hist.keys()), list(count_hist.values()))
        plt.title(title)
        plt.xlabel('Number of CALL_TYPES')
        plt.ylabel('Frequency')
        if save_fig:
            plt.savefig("./plots/plot_call_type_train.png")
        plt.show()

    @staticmethod
    def plot_linear_regression_2_best_coords(best_distances):
        fig = plt.figure()
        ax = fig.add_subplot(111)

        ax.bar(list(range(len(best_distances))), best_distances)
        plt.title(f"Test prediction error - MEAN: {round(np.mean(best_distances), 2)} meters")
        plt.xlabel("Taxi rides")
        ax.annotate('Max error', xy=(np.argmax(best_distances), np.max(best_distances)),
                    xytext=(np.argmax(best_distances) - 15, np.max(best_distances) - 15),
                    arrowprops=dict(facecolor='black', shrink=0.05),
                    )
        ax.annotate('Min error', xy=(np.argmin(best_distances), np.min(best_distances)),
                    xytext=(np.argmin(best_distances) + 3, np.min(best_distances) + 25),
                    arrowprops=dict(facecolor='black', shrink=0.05),
                    )
        plt.ylabel("Distance meters")
        plt.show()

    @staticmethod
    def plot_linear_regression_best_distances(values_min_points, means):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(values_min_points, means)
        plt.title("Mean distance error")
        plt.xlabel("Number of minimum points")
        plt.ylabel("Error in meters")
        plt.show()
    @staticmethod
    def plot_random_forest_results(predicted_values: np.array, actual_values: DataFrame, accuracy: float,
                                   save_fig: bool = False):
        """
        Plots the result of the random forest ML algorithm


        :param predicted_values: Predicted values of the model
        :type: predicted_values: array
        :param actual_values: Actual values of the dataset
        :type: actual_values: DataFrame
        :param accuracy: Float number
        :type: accuracy: float
        :param save_fig:
        :return: None
        """
        x = list(range(len(predicted_values)))
        plt.title("Random forest results accuracy {}".format(round(accuracy, 2)))
        plt.xlabel("Number of samples")
        plt.ylabel("Target value")
        plt.plot(x, predicted_values, label="Predicted values")
        plt.plot(x, actual_values.values, label="Actual values")
        plt.legend(loc='upper left')
        if save_fig:
            plt.savefig("./plots/plot_random_forest.png")
        plt.show()

    def __del__(self):
        print(f"Destruction of {self.__class__.__name__}")
        self.dataset = None
